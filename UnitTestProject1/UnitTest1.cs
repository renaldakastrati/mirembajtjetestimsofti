﻿using System;
using WebApplicationHumanResources;
using WebApplicationHumanResources.Controllers;
using Xunit;
using Moq;
using System.Web;
using System.Web.Mvc;

namespace UnitTestProject1
{
    public class UnitTest1
    {
        var controller = new UsersController();
        [Fact]
        public void UserLoginTest_isAdmin_ReturnsAction()
        {
            //var optionsBuilder = new DETYRAEntities();
            //optionsBuilder.UseInMemoryDatabase();
            User user = new User { FirstName = "reni", Password = "123456", RoleId = 1 };
            var result = controller.Login(user) as ViewResult;
            Assert.Equal("adminPage", result.ViewName);
        }
        [Fact]
        public void details_isvalid_returnstoaction()
        {
            var result = controller.Details(8) as ViewResult;
            var detail = (User)result.ViewData.Model;
            Assert.Equal("reni", detail.FirstName);
        }

        [Fact]
        public void Get_non_existent_user_returns_null()
        {
            // Act
            var result = controller.GetPerson("Fred") as ViewResult;

            // Assert
            Assert.Null(result.ViewData.Model);
        }

        //var result = (RedirectToRouteResult) user.Login(-1);
        //Assert.AreEqual("Login", result.Values["action"]);
        ////Arrange


        //act
        //  var result =  controller.Login(new User { RoleId = 2 });

        //assert
    }
}
