﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplicationHumanResources;

namespace WebApplicationHumanResources.Controllers
{
    public class UsersController : Controller
    {
        private DETYRAEntities db = new DETYRAEntities();
        
        public ActionResult Employees() {
           
                
            var id = (int)Session["UserID"];
            var s = (from e in db.Users 
                     where e.SupervizorId == id
                     select e).AsEnumerable().ToList();

            return View(s);
        }

    

        // GET: Users
        public ActionResult Index()
        {
            var users = db.Users.Include(u => u.User2);
            return View(users.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            var u = new User();
            ViewBag.SupervizorId = new SelectList(db.Users, "UserID", "FirstName");
            return View(u);
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( User user)
        {
                var u = new User();
                if (ModelState.IsValid)
                {
                    u.FirstName = user.FirstName;
                    u.LastName = user.LastName;
                    u.Username = user.Username;
                    u.RoleId = user.RoleId;
                    u.Password = user.Password;
                    if (user.RoleId == 2)
                    {
                        u.SupervizorId = null;
                    }
                    else if (user.RoleId == 3)
                    {
                        u.SupervizorId = user.SupervizorId;

                    }
                    db.Users.Add(user);
                    db.SaveChanges();
                    return RedirectToAction("RequestByUserLogedIn");

                }

                ViewBag.SupervizorId = new SelectList(db.Users, "UserID", "FirstName", user.SupervizorId);
               return View(user);
           
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.SupervizorId = new SelectList(db.Users, "UserID", "FirstName", user.SupervizorId);
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserID,FirstName,LastName,RequestId,Username,RoleId,SupervizorId,Password")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SupervizorId = new SelectList(db.Users, "UserID", "FirstName", user.SupervizorId);
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
