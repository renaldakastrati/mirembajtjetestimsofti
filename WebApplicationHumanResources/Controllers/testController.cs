﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplicationHumanResources;

namespace WebApplicationHumanResources.Controllers
{
    public class testController : ApiController
    {
        private DETYRAEntities db = new DETYRAEntities();

        //// GET: api/test
        //public string GetRequests()
        //{
        //    return "hello Renalda";// db.Requests;
        //}

        public List<Request> GetRequest()
        {
            List<Request> requests = (from re in db.Requests
                            select re).ToList();

            return requests;
        }

        // GET: api/test/5
        [ResponseType(typeof(Request))]
        public string GetRequest(int id)
        {
            //Request request = db.Requests.Find(id);
            //if (request == null)
            //{
            //    return NotFound();
            //}

            //return Ok(request);
            return "hello Renalda! Id is = " + id; 
        }

        // PUT: api/test/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRequest(int id, Request request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != request.RequestID)
            {
                return BadRequest();
            }

            db.Entry(request).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/test
        [ResponseType(typeof(Request))]
        public IHttpActionResult PostRequest(Request request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Requests.Add(request);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = request.RequestID }, request);
        }

        // DELETE: api/test/5
        [ResponseType(typeof(Request))]
        public IHttpActionResult DeleteRequest(int id)
        {
            Request request = db.Requests.Find(id);
            if (request == null)
            {
                return NotFound();
            }

            db.Requests.Remove(request);
            db.SaveChanges();

            return Ok(request);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RequestExists(int id)
        {
            return db.Requests.Count(e => e.RequestID == id) > 0;
        }
    }
}