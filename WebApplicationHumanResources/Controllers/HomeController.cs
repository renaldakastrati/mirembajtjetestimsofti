﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplicationHumanResources.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User objUser)
        {
            if (ModelState.IsValid)
            {
                using (DETYRAEntities db = new DETYRAEntities())
                {
                    var obj = db.Users.Where(a => a.Username.Equals(objUser.Username) && a.Password.Equals(objUser.Password)).FirstOrDefault();

                    if (obj != null)
                    {
                        Session["UserID"] = obj.UserID;
                        Session["Username"] = obj.Username;


                        if (obj.RoleId == 1)
                        {
                            return RedirectToAction("adminPage");
                        }
                            
                        else if (obj.RoleId == 2)
                        {

                            return RedirectToAction("supervizorPage", Session["UserID"]);

                        }
                        else if (obj.RoleId == 3)
                        {
                            return RedirectToAction("employeePage");
                        }

                    }
                }
            }
            return View(objUser);
        }

        public ActionResult adminPage()
        {

            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }


        public ActionResult supervizorPage()
        {
            using (DETYRAEntities db = new DETYRAEntities())
            {
                if (Session["UserID"] != null)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Login");
                }
            }
        }

        public ActionResult employeePage()
        {
            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }



        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Login", "Home");
        }
    }
}